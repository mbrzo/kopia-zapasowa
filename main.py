import pickle
import os
from tkinter import *
from tkinter import filedialog, messagebox
from shutil import copy2

def print_text(text, append=True):
    global text_var, current_text
    # function prints out informations in the label below the iterface
    # if not in Append mode - clear the text
    if not append:
        current_text = []
    # add passed text to the full message list of lines
    current_text.append(text)
    # Check if number of lines is under o eqal to 25, if it is more - delete the top (first) line
    if len(current_text) > 24:
        del current_text[0]

    # Add every line into the string that will be put into the text label
    temp_str = ""
    for l in current_text:
        temp_str += l
        temp_str += "\n"
    # Fill in the difference between 25 and no. of lines with newlines
    if len(current_text) < 24:
        for i in range(24-len(current_text)):
            temp_str += "\n "

    # Put the temporary text string into the text label
    text_var.set(temp_str)

def print_lb():
    print_text('--------------------------------------------------------------------------------------------------------------------------------')
    print_text(' ')

def print_top_info():
    global directories_indexed, indexes
    # prints information about indexed dirs. etc..
    print_text('Kopia Zapasowa Plików. Wersja 1.0.  2020', append=False)
    print_text(' ')
    print_text('{} indexowanych katalogów.'.format(len(directories_indexed.keys())))
    print_text('Łączna ilość zaindexowanych plików: {}'.format(len(indexes)))
    print_lb()

def make_a_copy():
    # Main function - makes a copy of files previously found by search_for_changes()
    global new_files, indexes
    #print(len(new_files))
    # If there are new files found:
    if new_files:
        print_top_info()
        print_text('Kopiuję nowe pliki do folderu kopii ...')
        for k, v in new_files.items():
            # For every new file, chceck if a copy dir. exists, if no - create it
            directory = os.path.dirname(v)
            if not os.path.exists(directory):
                os.makedirs(directory)
            # Make an actual copy
            copy2(k, v)
            # Add the copied file into the index.
            indexes.append(v)
    else:
        messagebox.showinfo("Brak plików", "brak plików do skopiowania,\nużyj opcji 'skanuj'")
    # zapisz ustawienia po wykonaniu kopii
    print_text('Zakończono')
    print_text('Utworzono kopię dla {} nowych plików.'.format(len(new_files.keys())))
    # When done - clear the new files
    new_files = {}
    save_settings()

def re_index():
    print_top_info()
    print_text('Indeksowanie plików w folderze kopii zapasowej:')
    global directories_indexed, indexes
    indexes = []
    directories_indexed = {}
    count = 0
    # Go through lines in input_listbox:
    for line in input_listbox.get(0, END):
        
        dirname = os.path.basename(line)
        #create a fullpath for output dir.
        copypath = os.path.join(os.path.abspath(output_entry.get()), dirname)
        # If no such directory, create one and continue to the nex one
        if not os.path.isdir(copypath):
            os.makedirs(copypath)

        # os.walk and index the directory
        for root, dirs, files in os.walk(copypath):
           for name in files:
              path = os.path.join(root, name)
              indexes.append(os.path.abspath(path))
              count += 1
              #print(os.path.abspath(path))
        # put in dictionary - dir_name: path
        directories_indexed[dirname] = line
    print_text('zaindeksowano {} plików.'.format(count))

def search_for_changes():
    print_top_info()
    print_text('Rozpoczynam skanowanie w poszukiwaniu nowych plików:')
    count = 0
    global new_files, indexes, directories_indexed
    # If user selected to search for new files - execute:
    if chb_1_var.get():
        # new files dict = input_path: output_path
        new_files = {}
        copypath = os.path.abspath(output_entry.get())
        #print(directories_indexed)
        #print(indexes)
        
        # For every input dir - use os.walk and compare if file/dir exists in output dir:
        for dirname, dirpath in directories_indexed.items():
            # Execute os.walk, and compare all dirs and files:
            for root, dirs, files in os.walk(dirpath):
                for name in files:
                    inputpath = os.path.abspath(os.path.join(root, name))
                    checkpath = os.path.abspath(os.path.join(copypath, dirname,
                                                             os.path.relpath(inputpath, dirpath)))
                    #print('checking:')
                    #print(inputpath)
                    #print(checkpath)
                    if checkpath not in indexes:
                        new_files[inputpath] = checkpath
                        count += 1
    print_text('Znaleziono {} nowych plików'.format(count))
        #print(new_files.keys())

def save_settings():
    print_top_info()
    print_text('Zapisuję ustawienia oraz listę plików kopii zapasowej...')
    # Gather all of the options
    settings = {}
    settings['output_entry'] = output_entry.get()
    settings['chb_1_var'] = chb_1_var.get()
    settings['chb_2_var'] = chb_2_var.get()
    settings['input_listbox'] = input_listbox.get(0, END)
    settings['directories_indexed'] = directories_indexed

    # save to file
    with open('settings.p', 'wb') as file:
        pickle.dump(settings, file, protocol=pickle.HIGHEST_PROTOCOL)
    # save indexes to file
    with open('indexes.p', 'wb') as file:
        pickle.dump(indexes, file, protocol=pickle.HIGHEST_PROTOCOL)
    #print('saving:\n\n')
    #print(settings)
    #print(indexes)
    print_text('[Zakończono]')

def load_settings():
    global indexes
    # Read settings from the file
    with open('settings.p', 'rb') as file:
        settings = pickle.load(file)
    # read indexes into the dict.:
    with open('indexes.p', 'rb') as file:
        indexes = pickle.load(file)

    # Insert values into tkinter boxes
    output_entry.insert(0, settings['output_entry'])
    chb_1_var.set(settings['chb_1_var'])
    chb_2_var.set(settings['chb_2_var'])
    if settings['input_listbox']:
        for item in settings['input_listbox']:
            input_listbox.insert(END, item)
    for k, v in settings['directories_indexed'].items():
        directories_indexed[k] = v
    #print('loading:\n\n')
    #print(settings)
    #print(indexes)

def outputSelect():
    # Selecting output directory from filedialog
    directory = filedialog.askdirectory()
    output_entry.delete(0, END)
    output_entry.insert(0, directory)

def inputSelect():
    # Selecting input directory from filedialog
    directory = filedialog.askdirectory()
    input_entry.delete(0, END)
    input_entry.insert(0, directory)

def inputAdd():
    # Adds anything that is in the entry box to the list box
    directory = input_entry.get()
    if directory != '':
        input_listbox.insert(END, directory)
        input_entry.delete(0, END)

def inputDelete():
    # Deletes selected line from the listbox
    sel = input_listbox.curselection()
    if sel:
        # Ask user for confirmation
        if messagebox.askyesno("Usunięcię pozycji","Czy na pewno chcesz usunąć:\n{}".format(input_listbox.get(sel))):
            input_listbox.delete(sel)

# Variables
directories_indexed = {}
indexes = []
new_files = {}
current_text = []

# -- TKINTER --

# Window object
window = Tk()
window.resizable(False, False)

# Make frames for input, for buttons and for text
top_frame = Frame(window)
options_frame = LabelFrame(top_frame)
button_frame = LabelFrame(top_frame)
input_frame = LabelFrame(top_frame)

top_frame.pack(fill=X)
options_frame.pack(side=LEFT, fill=Y)
button_frame.pack(side=LEFT, fill=Y)
input_frame.pack(side='left', fill=Y)

# make and pack option boxes and checkboxes:
output_label = Label(options_frame, text = 'Folder kopii:')
output_entry = Entry(options_frame)
output_sel_button = Button(options_frame, text='...', command=outputSelect)
chb_1_var = IntVar()
chb_1 = Checkbutton(options_frame, text = 'Szukaj nowych plików', variable=chb_1_var)
chb_2_var = IntVar()
chb_2 = Checkbutton(options_frame, text = 'Szukaj zmodyfikownych plików', variable=chb_2_var, state=DISABLED)

output_label.grid(row = 0, column = 0, sticky = W)
output_entry.grid(row = 0, column = 1, sticky = W)
output_sel_button.grid(row=0, column=2, sticky=W)
chb_1.grid(row = 1, column = 0, columnspan=2, sticky = W)
chb_2.grid(row = 2, column = 0, columnspan=2, sticky = W)

# Make the buttons:
button_search = Button(button_frame, text = 'Skanuj', command=search_for_changes)
button_make_copy = Button(button_frame, text = 'Utwórz kopię', command=make_a_copy)
button_save_settings = Button(button_frame, text = 'Zapisz', command=save_settings)

button_search.pack()
button_make_copy.pack()
button_save_settings.pack(side='bottom')

# Make the input directories frame:
input_entry = Entry(input_frame)
input_label = Label(input_frame, text='Dodaj folder którego chcesz tworzyć kopie:')
input_add_button = Button(input_frame, text = 'Dodaj', command=inputAdd)
lb_scrollbar = Scrollbar(input_frame)
input_listbox = Listbox(input_frame, height=4, width=60, selectmode=SINGLE, yscrollcommand=lb_scrollbar.set)
input_delete_button = Button(input_frame, text = 'Usuń', command=inputDelete)
input_sel_button = Button(input_frame, text='...', command=inputSelect)
input_index_button = Button(input_frame, text='Indexuj', command=re_index)
lb_scrollbar.config(command=input_listbox.yview)

input_label.grid(row=0, column=0)
input_entry.grid(row=0, column=1)
input_add_button.grid(row=0, column=3)    
input_listbox.grid(row=1, column=0, rowspan=2, columnspan=2)
input_delete_button.grid(row=1, column=3)
lb_scrollbar.grid(row=1, column=2, rowspan=2, sticky=W+E+N+S)
input_sel_button.grid(row=0, column=2, sticky=W)
input_index_button.grid(row=2, column=3, sticky=S)

# Make the text field:
text_var = StringVar()
text_field = Label(window, relief='sunken', textvariable=text_var, width=100,
                   height=25)
text_field.pack()

# Load settings:
try:
    load_settings()
except FileNotFoundError:
    pass

print_top_info()

# Main loop
window.mainloop()
