# Kopia zapasowa

Aplikacja do tworzenia kopii plików 1:1 wybranych folderów w folderze docelowym.

## Funkcje:
*  Tworzenie kopii całych folderów (dosłowne kopiowanie plików do nowej lokalizacji).
*  Dodawanie nowych plików do istniejącego folderu kopii.

## Wymagania:
*  Python w wersji 3.x (testowano na wersji 3.7)

## How-to:
*  Konfiguracja
    1.  Uruchomić plik ".bat" lub "main.py"
    2.  W górnej części, w pierswszej ramce od lewej strony należy wskazać **folder gdzie zapisywana będzie kopia zapasowa** ("Folder kopii").  
        Aby wyszukać folder, można uzyć przycisku "...".
    3.  Poniżej należy wybrać metodę wyszukiwania plików.  
        (w obecnej wersji dostępna jedynie ocja wyszukiwania nowych plików)  
        ! Bez zaznaczonej żadnej opcji program nie wyszukażadnych plików
    4.  W sekcji z prawej strony, należy podać **foldery Których kopię chce się wykonać.**  
        Aby to zrobić, należy w górnym pasku wpisać lub wysukać przyciskiem "..." PEŁNĄ ścieżkę folderu,  
        a następnie kliknąć przycisk "dodaj".
      
        (Pozycję można również usunąć wybierając ją z listy i klikająć przycisk "usuń")
    5.  Kliknąć przycisk "zapisz"
*   Użytkowanie
    1.  Jeżeli nagłówek wyświetla informację "0 indexowanych katalogów" należy wybrać opcję "Indexuj"
    2.  Należy wybrać jedną lub kilka opcji wyszukiwania:
        - Szukaj nowych plików
        - Szukaj zmodyfikowanych plików
    3.  Wybrać opcję "Szukaj"  
        *Program przeszuka folder kopii w poszukiwaniu plików których niema w indeksie*
    4.  Po wyszukaniu program poda ilość znalezionych nowych plików.  
        Aby wykonać kopię należy wybrać opcję "utwórz kopię"